package model

import org.joda.time.DateTime
import play.api.libs.json.Json


case class Currency(id: Option[Long], created: DateTime, currency: String, rate: Double)

object CurrencyConvertions {
  implicit val currencyWrites = Json.writes[Currency]
}
