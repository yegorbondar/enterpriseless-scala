package model

import play.api.libs.json.Json


case class CurrenciesResponse(currencies: List[Currency])

object CurrenciesResponseConversions {

  import model.CurrencyConvertions.currencyWrites

  implicit val resp = Json.writes[CurrenciesResponse]
}