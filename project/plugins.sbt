// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.5.2")

// web plugins

addSbtPlugin("com.typesafe.sbt" % "sbt-less" % "1.1.0")

addSbtPlugin("com.typesafe.sbt" % "sbt-jshint" % "1.0.3")

addSbtPlugin("com.typesafe.sbt" % "sbt-rjs" % "1.0.7")

addSbtPlugin("com.typesafe.sbt" % "sbt-digest" % "1.1.0")

addSbtPlugin("org.irundaia.sbt" % "sbt-sassify" % "1.4.2")

// assembly plugin

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.11.2")

addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.1")

// OWASP security plugin
addSbtPlugin("net.vonbuchholtz" % "sbt-dependency-check" % "0.1.0")
